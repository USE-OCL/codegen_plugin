#!/bin/sh
##
## add_language.sh for add_language in /home/vincent/USE/codegen_plugin
##
## Made by
## <vincent.davoust@gmail.com>
##
## Started on  Thu May 10 16:55:04 2018
## Last update Thu May 31 09:39:37 2018 
##
genpath="org/tzi/use/codegen/"
pluginpath="org/tzi/use/gui/plugins/codegen/"

function languageExists {
    if [[ -f $pluginpath"Gen"$1".java" ||
	  -f $genpath$1"/"$1"Lang.java" ||
	  -f $genpath$1"/"$1"Writer.java" ||
	  -f $genpath$1"/"$1"ExpressionWriter.java" ]]
    then
	echo "Language"$1"already exists";
	exit;
    fi
}

function usePlugin {

    head -n-2 useplugin.xml > useplugin2.xml
    cat >> useplugin2.xml << EOF
	       <action label="Generate $1" icon="resources/$1Gen.png"
	       	       class="org.tzi.use.gui.plugins.codegen.$1.Gen$1"
		       tooltip="Generate Headers for Classes"
		       menu="Generate code from model"
		       menuitem="Generate $1 interface file"
		       toolbaritem="Header Generator"
		       id="org.tzi.use.gui.plugins.codegen.Gen$1">
		</action>
	</actions>
</plugin>
EOF

    mv useplugin2.xml useplugin.xml
}

language=$1

#languageExists $language

if [[ $language == "" ]]
then
    echo "Please specify a language. Example :" $0 "Java"
    exit;
fi

usePlugin $language
cp $pluginpath"GenExample.java_" $pluginpath"Gen"$language".java"
cp $genpath"Example/ExampleLang.java_" $genpath$language"/"$language"Lang.java"
cp $genpath"Example/ExampleWriter.java_" $genpath$language"/"$language"Writer.java"
cp $genpath"Example/ExampleExpressionWriter.java_" $genpath$language"/"$language"ExpressionWriter.java"

find . -iname "*"$language"*.java" -type f -exec sed -i 's/Example/'$language'/g' {} +
