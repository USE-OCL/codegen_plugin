#!/bin/sh
##
## clean.sh for clean in /home/vincent/USE/codegen_plugin
##
## Made by
## <vincent.davoust@gmail.com>
##
## Started on  Mon May 14 15:35:14 2018
## Last update Thu May 17 17:24:48 2018 
##



rm org/tzi/use/codegen/*.class
rm org/tzi/use/gui/plugins/codegen/*.class

for d in ./org/tzi/use/codegen/*/ ; do
    rm $d*.class
done;
