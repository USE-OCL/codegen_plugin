### README ###

A plugin to automatically generate headers/interfaces from USE class diagram

Maybe implemetation code can be generated from ocl, in a later update

### Adding a language ###
run the script `./add_language.sh LANGUAGE_NAME` to add a language to the project
This will add a menu entry, create a 'main' (org/tzi/use/gui/plugins/codegen/Gen<Language>.java), and create writers (org/tzi/use/codegen/<Language>*.java)
Feel free to add a 19x19px icon for your language in resources/<Language>Icon.gif 

### Where to start coding ###
# For UML #
When a button is clicked in the menu, one of org/tzi/use/gui/plugins/codegen/Gen<Language>.java:performAction(args) is called
This mehtod should create a writer for your language and call the generic use process with this writer.

This writer should be org/tzi/use/codegen/<Language>Writer.java. Please implement this file for generating code from UML.
For generating code from OCL, edit org/tzi/use/codegen/<Language>ExpressionWriter.java


Then, for OCL 


### Please contribute ###
Trello : https://trello.com/b/IySEWzcY/usecodegenplugin
Gitlab : https://gitlab.com/USE-OCL/codegen_plugin
