#include "libOCL.hpp"

using namespace PROJECT_NAMEocl;



template<class T>
void Collection<T>::forAll( void (*func)())
{
    for(int i = 0;i<this.size();i++){
        func(this[i]);
    }
}

template<typename T>
template<typename R>
bool Collection<T>::exists(bool (*func)())
{

    for(int i = 0;i<this.size();i++){
        if(func(this[i]))
            return true;
    }

   return false;
}
template<typename T>
bool Collection<T>::isUnique(bool (*func)())
{
    int nbr = 0;
    for(int i = 0;i<this.size();i++){
        if(func(this[i]))
            nbr++;
    }

    if(nbr==1){
        return true;
    }
   return false;
}




