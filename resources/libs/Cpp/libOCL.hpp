/*
** libOCL.hpp for libOCL in /home/vincent/USE/codegen_plugin
**
** Made by
** <vincent.davoust@gmail.com>
**
** Started on  Thu May 17 16:57:35 2018
** Last update Thu May 17 17:12:20 2018
*/
#include "stdlib.h"
#include <iostream>
#include <vector>
#ifndef LIBOCL_HPP_
# define LIBOCL_HPP_

// OCL types

namespace PROJECT_NAMEocl{

// Add aliases for USE type names to Cpp type names
typedef int		Integer;
typedef bool		Boolean;
typedef std::string	String;

template <typename T>
// Define collection types
class Collection : public std::vector<T> {
public:
   void forAll( void (*func)());
    bool exists(bool (*func)());
    bool isUnique(bool (*func)());

};
template <typename T>

class Sequence : public Collection<T> {

};
template <typename T>

class Set : public Collection<T> {

};
template <typename T>

class OrderedSet : public Collection<T> {

};
template <typename T>

class Bag : public Collection<T> {

};
class ContraintViolated : public std::exception{

};


// define operations

//  void AllInstances();
void Any();
void AsType();
void AttrOp();


//  void If();
void IsKindOf();
void IsTypeOf();
void IsUnique();
void Iterate();
void Let();
//  void Navigation();

void ObjAsSet();
void ObjOp();
void ObjRef();
//  void One();
//  void OrderedSetLiteral();
//  void Query();
void Reject();
//  void WithValue();
void Select();
//  void SequenceLiteral();
//  void SetLiteral();
void SortedBy();
//  void StdOp();
void TupleLiteral();
void TupleSelectOp();
//  void Undefined();
//  void Variable();
//  void Closure();
void OclInState();
//  void VarDeclList();
//  void VarDecl();
//  void ObjectByUseId();
//  void ConstUnlimitedNatural();
void SelectByKind();
void ExpSelectByType();
void Range();
void NavigationClassifierSource();

};

#endif /* !LIBOCL_HPP_ */

