/*
**
*/

package org.tzi.use.codegen;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.ocl.type.Type;
import java.util.HashMap;

import java.io.*;

public abstract class CodeWriter {

    protected enum Scope {PRIVATE, PUBLIC, PROTECTED, DEFAULT}

    protected String filename;
    protected String content;
    protected HashMap<String, String> language;


    protected Scope currentScope = Scope.DEFAULT;
    protected String fileTop = "";
    protected String includes = "";
    protected String getSet = "";
    protected String constructor = "";
    protected String destructor = "";
    protected String methodDefs = "";
    protected String publicStatements = "";
    protected String privateStatements = "";
    protected MClass thisClass;
    protected MModel thisModel;

    protected int tabs = 0;

    private String TAB_CHAR = "  ";

    public CodeWriter() {
    }

    public void newFile(String filename) {
	this.filename = filename;
	this.content = "";
    }


    public abstract void createProject(MSystem fSystem);

    public void beginFile(MModel fModel, MClass fClass) {

	this.fileTop =  language.get("COMMENT") + "\n" +
			language.get("COMMENT") + " Header file generated with USE codegen plugin\n"+
			language.get("COMMENT") + "\n\n";

	this.getSet = "";
	this.includes = "";
	this.constructor = "";
	this.destructor = "";
	this.methodDefs = "";
	this.publicStatements = "";
	this.privateStatements = "";
	this.thisClass = fClass;
	this.thisModel = fModel;

    }
    public abstract void endFile(MClass fClass);

    public abstract void beginClass(MClass fClass);
    public abstract void endClass(MClass fClass);

    public abstract void addOperation(MOperation fOperation);
    public abstract void addAssociation(MAssociation fAssociation);

    public abstract void writeImplFile(MClass fClass);


    public void write() {

	try {
	    FileOutputStream out = new FileOutputStream(filename);
	    out.write(content.getBytes());
	    out.close();
	} catch (IOException e) {
	    System.out.println("File write failed - " + filename);
	}
    }

    protected String getType(Type type) {
	String res = language.get("VOID");
	try {
	    if (type.isTypeOfInteger())
		res = language.get("INT");
	    else if (type.isTypeOfBoolean())
		res = language.get("BOOL");
	    else if (type.isTypeOfReal())
		res = language.get("REAL");
	    else if (type.isTypeOfString())
		res = language.get("STRING");
	    else {
		res = type.shortName();
		addInclude(res);
	    }
	} catch (Exception e) {
	    System.out.println("NO SUCH TYPE FOUND : " + type + " - using 'void'");
	    res = language.get("VOID");
	}

	return formatType(res);
    }

    protected String formatType(String s) {
//	String s2 = formatType(quoteContent(s), expr);
	String res = "";
	int subT_b = s.lastIndexOf('(');
	subT_b = (subT_b >=0)? subT_b+1 : 0;
	int subT_e = s.lastIndexOf(')');
	subT_e = (subT_e >=0)? subT_e : s.length();

	res = s.substring(subT_b, subT_e);

	System.out.println("Type : \t\t" + s);
	if	  (s.contains("Sequence(")) {
	    res = "ocl::Sequence<"+res+">";
	} else if (s.contains("Set(")) {
	    res = "ocl::Set<"+res+">";
	} else if (s.contains("Bag(")) {
	    res = "ocl::Bag<"+res+">";
	} else if (s.contains("Collection(")) {
	    res = "ocl::Collection<"+res+">";
	}
	return res;
    }
    protected void addInclude(String type) {
	System.out.println("WARNING : Do not know how to add include of unknown type " + type);
    }


    public void addAttribute(MAttribute fAttribute) {
	String name = fAttribute.name();
	String type = getType(fAttribute.type());
	privateStatements += language.get("DECL_VAR") +
	    "\t\t" + type + "\t" + name + language.get("END_LINE");

	String capitalName = Character.toUpperCase(name.charAt(0)) + name.substring(1);

	getSet += "\t\t"+ language.get("DECL_FUNC")+
	    type + "\tget" + capitalName +
	    language.get("ARG_OPEN") + language.get("ARG_CLOSE")+
	    language.get("BLOCK_OPEN")+
	    "\t\t\t"+language.get("RETURN") + " \t" + name + language.get("END_LINE")+
	    "\t\t"+language.get("BLOCK_CLOSE");

	getSet += "\t\t"+ language.get("DECL_FUNC")+
	    language.get("VOID") + "\tset" + capitalName +
	    language.get("ARG_OPEN")+type+" value"+ language.get("ARG_CLOSE")+
	    language.get("BLOCK_OPEN")+
	    "\t\t\t"+name+" = value"+ language.get("END_LINE")+
	    ""+language.get("BLOCK_CLOSE");
    }



    public String tabulate() {
	String result = new String(new char[this.tabs]).replace("\0", TAB_CHAR);//"\t"*this.tabs;
	/*	for (int i in range(this.tabs)) {
	    result += "\t";
	    } */
	return result;
    }

    public void addTab() {
	this.tabs++;
    }

    public void rmTab() {
	this.tabs--;
	if (this.tabs < 0)
	    this.tabs = 0;
    }
}
