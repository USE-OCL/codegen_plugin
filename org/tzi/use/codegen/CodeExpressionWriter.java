/*
 *
 */

package org.tzi.use.codegen;


import org.tzi.use.uml.ocl.type.Type;
import org.tzi.use.uml.ocl.expr.Expression;
import org.tzi.use.uml.ocl.expr.ExpressionPrintVisitor;
import org.tzi.use.uml.ocl.expr.*;
import org.tzi.use.uml.ocl.type.Type.VoidHandling;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;


import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;



public class CodeExpressionWriter implements ExpressionVisitor {

	protected  PrintWriter writer;

    protected HashMap<String,String> language;
    protected boolean isPostCondition = false;


	@Override
	public String toString(){
		return writer.toString();
	}

	protected String ws(){
		return " ";
	}


    public CodeExpressionWriter(HashMap lang) throws IOException{
	this.writer = new VirtualPrintWriter();
	language = lang;
	((VirtualPrintWriter)writer).setCommentString(language.get("COMMENT"));
	((VirtualPrintWriter)writer).setSelfString(language.get("THIS"));
    }

    public void setIsPostCondition(boolean postCondition) {
	isPostCondition = postCondition;
    }
	protected String quoteContent(String s) {
		return s;
	}

	protected  String keyword(String s, Expression expr){
		return formatKeyword(quoteContent(s), expr);
	}

	protected String formatKeyword(String s, Expression expr) {
		return s;
	}

	protected  String variable(String s, Expression expr){
		return formatVariable(quoteContent(s), expr);
	}

	protected String formatVariable(String s, Expression expr) {
		return s;
	}

	protected String type(String s, Expression expr){
		return formatType(quoteContent(s), expr);
	}


	protected String operation(String s, Expression expr){
		return formatOperation(quoteContent(s), expr);
	}

	protected String formatOperation(String s, Expression expr) {
		return s;
	}

	protected String operator(String s, Expression expr){
		return formatOperator(quoteContent(s), expr);
	}

	protected String formatOperator(String s, Expression expr) {
		return s;
	}


	protected String formatLiteral(String s, Expression expr) {
		return s;
	}
    public void visitConstString(ExpConstString exp) {
	writer.write("\"");
	writer.write(literal(exp.value(), exp));
	writer.write("\"");
    }

    protected void atPre(Expression exp) {
	if(exp.isPre()){
	    writer.write(operator("@pre", exp));
	}
    }

    @Override
    public void visitIf(ExpIf exp) {
	((VirtualPrintWriter)writer).writeExec(language.get("IF"));
	((VirtualPrintWriter)writer).writeExec(ws() + language.get("COND_OPEN"));
	exp.getCondition().processWithVisitor(this);
	((VirtualPrintWriter)writer).writeExec(language.get("COND_CLOSE") + ws());
	((VirtualPrintWriter)writer).writeExec(language.get("BLOCK_OPEN"));
	exp.getThenExpression().processWithVisitor(this);
	((VirtualPrintWriter)writer).writeExec(language.get("BLOCK_CLOSE"));
	((VirtualPrintWriter)writer).writeExec(language.get("ELSE"));
	((VirtualPrintWriter)writer).writeExec(language.get("BLOCK_OPEN"));
	exp.getElseExpression().processWithVisitor(this);
	((VirtualPrintWriter)writer).writeExec(language.get("BLOCK_CLOSE"));
    }



    @Override
    public void visitVarDecl(VarDecl varDecl) {
	boolean isOwnLine = true;
	if (((VirtualPrintWriter)writer).lastLine().contains(language.get("ARG_OPEN")))
	    isOwnLine = false;
	((VirtualPrintWriter)writer).writeExec(language.get("DECL_VAR") + " ");
	((VirtualPrintWriter)writer).writeExec(getType(varDecl.type()));
	if (isOwnLine)
	    ((VirtualPrintWriter)writer).writeExec("\t");
	else
	    ((VirtualPrintWriter)writer).writeExec(" ");
	((VirtualPrintWriter)writer).writeExec(variable(varDecl.name(), null));
	if (isOwnLine)
	    ((VirtualPrintWriter)writer).writeExec(language.get("END_LINE"));
    }

    @Override
    public void visitVarDeclList(VarDeclList varDeclList) {
	for (int i = 0; i < varDeclList.size(); ++i) {
	    if (i > 0) {
		((VirtualPrintWriter)writer).writeExec("\n");
	    }

	    varDeclList.varDecl(i).processWithVisitor(this);
	}
    }


    protected String formatType(String s, Expression expr) {
	return formatType(s);
    }
    protected String formatType(String s) {
//	String s2 = formatType(quoteContent(s), expr);
	String res = "";
	int subT_b = s.lastIndexOf('(');
	subT_b = (subT_b >=0)? subT_b+1 : 0;
	int subT_e = s.lastIndexOf(')');
	subT_e = (subT_e >=0)? subT_e : s.length();

	res = s.substring(subT_b, subT_e);

	System.out.println("Type : \t\t" + s);
	if	  (s.contains("Sequence(")) {
	    res = "ocl::Sequence<"+res+">";
	} else if (s.contains("Set(")) {
	    res = "ocl::Set<"+res+">";
	} else if (s.contains("Bag(")) {
	    res = "ocl::Bag<"+res+">";
	} else if (s.contains("Collection(")) {
	    res = "ocl::Collection<"+res+">";
	}
	return res;
    }
	@Override
	public void visitEmptyCollection(ExpEmptyCollection exp) {
	    writer.write("ocl::");
	    writer.write(operation(exp.name(), exp));
	    writer.write(operator("<", exp));
	    writer.write(type(exp.type().toString(), exp));
	    writer.write(operator(">", exp));
	}


    protected String getType(Type type) {
	// remove duplicate code with CodeWriter !!!
	String res = language.get("VOID");
	try {
	    if (type.isTypeOfInteger())
		res = language.get("INT");
	    else if (type.isTypeOfBoolean())
		res = language.get("BOOL");
	    else if (type.isTypeOfReal())
		res = language.get("REAL");
	    else if (type.isTypeOfString())
		res = language.get("STRING");

	    else if (type.isKindOfBag(VoidHandling.EXCLUDE_VOID))
		res = language.get("BAG");
	    else if (type.isKindOfSequence(VoidHandling.EXCLUDE_VOID))
		res = language.get("SEQUENCE");
	    else if (type.isKindOfSet(VoidHandling.EXCLUDE_VOID))
		res = language.get("SET");
	    else if (type.isKindOfCollection(VoidHandling.EXCLUDE_VOID))
		res = language.get("COLLECTION");

	    else {
		res = type.shortName();
	    }
	} catch (Exception e) {
	    System.out.println("NO SUCH TYPE FOUND : " + type + " - using 'void'");
	    res = language.get("VOID");
	}

	//	System.out.println("Type : \t" + res);
	return formatType(res);

    }

    private int depthLevel = 0;
    @Override
    public void visitStdOp(ExpStdOp exp) {
	boolean isOwnLine = false;
	if (((VirtualPrintWriter)writer).lastChar() == '\n') {
	    isOwnLine = true;
	    ((VirtualPrintWriter)writer).writeExec("\t");
	}

	//x	System.out.println("expression : " + exp.name());

	depthLevel++;
	superVisitStdOp(exp);
	depthLevel--;

	/* If this is a postCondition, replace '=' with '==' */
	if (isPostCondition) {
	    ((VirtualPrintWriter)writer).replaceInLastLine("[=]+", "==");
	}

	/* FORMAT CODE : write new/end line if needed, return statement if needed...*/
	char c_first = ((VirtualPrintWriter)writer).lastLine().replaceAll("\\s+","").charAt(0);
	char c_last = ((VirtualPrintWriter)writer).lastChar();
	if (depthLevel == 0 &&
	    (c_first == language.get("ARG_OPEN").charAt(0) &&
	     c_last == language.get("ARG_CLOSE").charAt(0))) {
	    ((VirtualPrintWriter)writer).prependLastLine("\n\t"+language.get("RES_EQ")+" ");
	    ((VirtualPrintWriter)writer).writeExec(language.get("END_LINE"));
	}

    }

    	protected void superVisitStdOp(ExpStdOp exp) {
		Expression[] args = exp.args();
		String operationName;

		if(exp.getOperation().isInfixOrPrefix()){
			operationName = keyword(exp.opname(), exp);
			if(args.length == 1){
				writer.write(operationName);
				writer.write(ws());
				args[0].processWithVisitor(this);
			} else { // Infix has two arguments
				writer.write(operator("(", exp));
				args[0].processWithVisitor(this);
				writer.write(ws());
				writer.write(operationName);
				writer.write(ws());
				args[1].processWithVisitor(this);
				writer.write(operator(")", exp));
			}
		} else {
			operationName = operation(exp.opname(), exp);
			if(exp.isPre()){
				operationName += operator("@pre", exp);
			}

			if(args.length == 0){
				writer.write(operationName);
			} else {
				args[0].processWithVisitor(this);
				writer.write(operator( args[0].type().isKindOfCollection(VoidHandling.EXCLUDE_VOID) ? "->" : ".", exp ));
				writer.write(operationName);
				writer.write(operator("(", exp));
				if(args.length > 1){
					for(int i = 1; i < args.length; i++){
						if(i > 1){
							writer.write(",");
							writer.write(ws());
						}
						args[i].processWithVisitor(this);
					}
				}
				writer.write(operator(")", exp));
			}
		}
	}

    protected String literal(String s, Expression expr){
	String res = formatLiteral(quoteContent(s), expr);
	if (((VirtualPrintWriter)writer).lastChar() == '\n')
	    res = "\t"+language.get("RES_EQ") + " \t" + res + language.get("END_LINE");
	return res;
    }

    protected void cast(ExpAsType exp) {
	writer.write(operator(language.get("ARG_OPEN"), exp));
	writer.write(operator(language.get("ARG_OPEN"), exp));
	writer.write(type(exp.getTargetType().toString(), exp));
	writer.write(operator(language.get("ARG_CLOSE"), exp));

	exp.getSourceExpr().processWithVisitor(this);
	writer.write(operator(language.get("ARG_CLOSE"), exp));
    }

    	@Override
	public void visitObjOp(ExpObjOp exp) {
	    exp.getArguments()[0].processWithVisitor(this);
	    writer.write(language.get("LINK_OP"));
	    writer.write(operation(exp.getOperation().name(), exp));
	    //	    atPre(exp);
	    writer.write(operator(language.get("ARG_OPEN"), exp));
	    for (int i = 1; i < exp.getArguments().length; ++i) {
		if (i > 1) {
		    writer.write(language.get("VAR_SEPR"));
		    writer.write(ws());
		}

		exp.getArguments()[i].processWithVisitor(this);
	    }
	    writer.write(operator(language.get("ARG_CLOSE"), exp));

	    // check if isOwnLine, then add ';'
	}





        /*------- TODO ---------*/

    	public void visitCollectionLiteral(ExpCollectionLiteral exp) {
		writer.write(keyword(exp.getKind(), exp));
		writer.write(operator("{", exp));

		boolean first = true;
		for (Expression elemExp : exp.getElemExpr()) {
			if (!first) {
				writer.write(",");
				writer.write(ws());
			}
			elemExp.processWithVisitor(this);
			first = false;
		}

		writer.write(operator("}", exp));
	}

    @Override
    public void visitAllInstances(ExpAllInstances exp) {
	// Call to allInstances in libOCL

	  writer.write(type(exp.getSourceType().toString(), exp));
	  writer.write(".");
	  writer.write(operation(exp.name(), exp));
	  atPre(exp);
	  writer.write(operator("()", exp));

    }



	@Override
	public void visitAny(ExpAny exp) {
	    // CAll to Any in libOCL
	    visitQuery(exp);
	}



	@Override
	public void visitAttrOp(ExpAttrOp exp) {

	      exp.objExp().processWithVisitor(this);
	      writer.write('.');
	      writer.write(exp.attr().name());
	      atPre(exp);

	}

	@Override
	public void visitBagLiteral(ExpBagLiteral exp) {
	    	visitCollectionLiteral(exp);
	}

	@Override
	public void visitCollect(ExpCollect exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitCollectNested(ExpCollectNested exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitConstBoolean(ExpConstBoolean exp) {
	    	writer.write(literal(String.valueOf(exp.value()), exp));
	}

	@Override
	public void visitConstEnum(ExpConstEnum exp) {

	      writer.write(type(exp.type().toString(), exp));
	      writer.write("::");
	      writer.write(literal(exp.value(), exp));

	}

	@Override
	public void visitConstInteger(ExpConstInteger exp) {
	    	writer.write(literal(String.valueOf(exp.value()), exp));
	}

	@Override
	public void visitConstReal(ExpConstReal exp) {
	    	writer.write(literal(String.valueOf(exp.value()), exp));
	}

	@Override
	public void visitExists(ExpExists exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitForAll(ExpForAll exp) {
	    	visitQuery(exp);
	}


	@Override
	public void visitIsKindOf(ExpIsKindOf exp) {

		exp.getSourceExpr().processWithVisitor(this);
		writer.write(".");
		writer.write(operation(exp.name(), exp));
		writer.write(operator("(", exp));
		writer.write(type(exp.getTargetType().toString(), exp));
		writer.write(operator(")", exp));

	}

	@Override
	public void visitIsTypeOf(ExpIsTypeOf exp) {

		exp.getSourceExpr().processWithVisitor(this);

		if (exp.getSourceExpr().type().isKindOfCollection(VoidHandling.EXCLUDE_VOID))
        	writer.write("->");
        else
        	writer.write(".");

		writer.write(operation(exp.name(), exp));
		writer.write(operator("(", exp));
		writer.write(type(exp.getTargetType().toString(), exp));
		writer.write(operator(")", exp));

	}

	@Override
	public void visitIsUnique(ExpIsUnique exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitIterate(ExpIterate exp) {
	    	visitQuery(exp, exp.getAccuInitializer());
	}

	@Override
	public void visitLet(ExpLet exp) {

		writer.write(operator("(", exp));
		writer.write(keyword("let", exp));
		writer.write(ws());
		writer.write(variable(exp.getVarname(), exp));
		writer.write(operator(":", exp));
		writer.write(type(exp.getVarType().toString(), exp));
		writer.write(ws());
		writer.write(operator("=", exp));
		writer.write(ws());
		exp.getVarExpression().processWithVisitor(this);
		writer.write(ws());
		writer.write(keyword("in", exp));
		writer.write(ws());
		exp.getInExpression().processWithVisitor(this);
		writer.write(operator(")", exp));

	}

	@Override
	public void visitNavigation(ExpNavigation exp) {

		exp.getObjectExpression().processWithVisitor(this);
		writer.write('.');
		writer.write(exp.getDestination().nameAsRolename());

		MAssociation assoc = exp.getDestination().association();
		MClass src = exp.getSource().cls();
		MClass dest = exp.getDestination().cls();
		int endsRequired = src.equals(dest) ? 2 : 1;

		if(assoc.associationEndsAt(src).size() > endsRequired){
			// check necessity for specifying the source role
			writer.write('[');
			writer.write(exp.getSource().nameAsRolename());
			writer.write(']');
		}
		else if(exp.getQualifierExpression().length > 0){
			// qualifier values
			writer.write('[');
			boolean first = true;
			for(Expression e : exp.getQualifierExpression()){
				if(!first){
					writer.write(',');
					writer.write(ws());
				}
				e.processWithVisitor(this);
				first = false;
			}
			writer.write(']');
		}
		atPre(exp);

	}

	@Override
	public void visitNavigationClassifierSource(ExpNavigationClassifierSource exp) {

		exp.getObjectExpression().processWithVisitor(this);
		writer.write('.');
		writer.write(exp.getDestination().nameAsRolename());
		atPre(exp);

	}

	@Override
	public void visitObjAsSet(ExpObjAsSet exp) {
	    	exp.getObjectExpression().processWithVisitor(this);
	}


	@Override
	public void visitObjRef(ExpObjRef exp) {
	    		writer.write(literal(exp.toString(), exp));
	}

	@Override
	public void visitOne(ExpOne exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitOrderedSetLiteral(ExpOrderedSetLiteral exp) {
	    	visitCollectionLiteral(exp);
	}

	public void visitQuery(ExpQuery exp, VarInitializer accuInitializer) {

		exp.getRangeExpression().processWithVisitor(this);
		writer.write(operator(".", exp));
		writer.write(operation(exp.name(), exp));
		writer.write(operator("(", exp));
		writer.write(ws());
		exp.getVariableDeclarations().processWithVisitor(this);
		if (accuInitializer != null) {
			writer.write(operator(";", exp));
			writer.write(ws());
			accuInitializer.getVarDecl().processWithVisitor(this);
			writer.write(operator("=", exp));
			accuInitializer.initExpr().processWithVisitor(this);
		}
		writer.write(ws());
		writer.write(operator("|", exp));
		writer.write(ws());
		exp.getQueryExpression().processWithVisitor(this);
		writer.write(ws());
		writer.write(operator(")", exp));

	}

	@Override
	public void visitQuery(ExpQuery exp) {
	    	visitQuery(exp, null);
	}

	@Override
	public void visitReject(ExpReject exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitWithValue(ExpressionWithValue exp) {
	    	writer.write(exp.getValue().toString());
	}

	@Override
	public void visitSelect(ExpSelect exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitSequenceLiteral(ExpSequenceLiteral exp) {
	    	visitCollectionLiteral(exp);
	}

	@Override
	public void visitSetLiteral(ExpSetLiteral exp) {
	    	visitCollectionLiteral(exp);
	}

	@Override
	public void visitSortedBy(ExpSortedBy exp) {
	    	visitQuery(exp);
	}


    	@Override
	public void visitTupleLiteral(ExpTupleLiteral exp) {

		writer.write(keyword("Tuple", exp));
		writer.write(operator("{", exp));
		boolean first = true;
		for(ExpTupleLiteral.Part p : exp.getParts()){
			if(!first){
				writer.write(operator(",", exp));
				writer.write(ws());
			}
			writer.write(p.getName());
			writer.write(operator("=", exp));
			p.getExpression().processWithVisitor(this);
			first = false;
		}
		writer.write(operator("}", exp));

	}

	@Override
	public void visitTupleSelectOp(ExpTupleSelectOp exp) {

		exp.getTupleExp().processWithVisitor(this);
		writer.write(".");
		writer.write(exp.getPart().name());

	}

	@Override
	public void visitUndefined(ExpUndefined exp) {
	    	writer.write(literal("null", exp));
	}

	@Override
	public void visitVariable(ExpVariable exp) {
	      writer.write(variable(exp.getVarname(), exp));
	}

	@Override
	public void visitClosure(ExpClosure exp) {
	    	visitQuery(exp);
	}

	@Override
	public void visitOclInState(ExpOclInState exp) {

		exp.getSourceExpr().processWithVisitor(this);
		writer.write(".");
		writer.write(operation(exp.name(), exp));
		writer.write(operator("(", exp));
		writer.write(exp.getState().name());
		writer.write(operator(")", exp));

	}


    	@Override
	public void visitObjectByUseId(ExpObjectByUseId expObjectByUseId) {

		writer.write(expObjectByUseId.getSourceType().name());
		writer.write(".");
		writer.write(operation(expObjectByUseId.name(), expObjectByUseId));
		writer.write(operator("(", expObjectByUseId));
		expObjectByUseId.processWithVisitor(this);
		writer.write(operator(")", expObjectByUseId));
		atPre(expObjectByUseId);

	}

	@Override
	public void visitConstUnlimitedNatural(
			ExpConstUnlimitedNatural expressionConstUnlimitedNatural) {
	    	writer.write("*");
	}

	@Override
	public void visitSelectByKind(ExpSelectByKind expSelectByKind) {

		expSelectByKind.getSourceExpression().processWithVisitor(this);
		writer.write(operator("->", expSelectByKind));
		writer.write(operation(expSelectByKind.name(), expSelectByKind));
		writer.write(operator("(", expSelectByKind));
		writer.write(type(expSelectByKind.type().elemType().toString(), expSelectByKind));
		writer.write(operator(")", expSelectByKind));

	}

	@Override
	public void visitExpSelectByType(ExpSelectByType expSelectByType) {
	    	visitSelectByKind(expSelectByType);
	}

	@Override
	public void visitRange(ExpRange exp) {

		exp.getStart().processWithVisitor(this);
		writer.write("..");
		exp.getEnd().processWithVisitor(this);

	}

    	public void visitAsType(ExpAsType exp) {
	    cast(exp);
	}


}
