/*
**
*/

package org.tzi.use.codegen;


import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;



public class VirtualPrintWriter extends PrintWriter {

    private String content = "";
    private String comment_str = "//";
    private String self_str = "this";

    public VirtualPrintWriter() throws IOException {
	super(new PrintWriter(new File("tmpExprBuffer.dat")));
    }

    public void setCommentString(String comment_string) {
    	comment_str = comment_string;
    }
    public void setSelfString(String self_string) {
    	self_str = self_string;
    }

    @Override
    public String toString() {
	return content;
    }

    public char lastChar() {
	if (content.length() < 1)
	    return '\0';

	return content.charAt(content.length() - 1);
    }

    public String lastLine() {
	int idx = content.lastIndexOf('\n');
	idx = (idx >=0)? idx : 0;
	return content.substring(idx + 1);
    }

    public void replaceInLastLine(String old_str, String new_str) {
	int idx = content.lastIndexOf('\n');
	idx = (idx >=0)? idx : 0;
	String end =  content.substring(idx);
	String begin = content.substring(0, idx);
	content = begin + end.replaceAll(old_str, new_str);
    }

    public void prependLastLine(String s) {
	int idx = content.lastIndexOf('\n');
	idx = (idx >=0)? idx : 0;
	content = content.substring(0, idx) + s + lastLine();
    }


    @Override
    public void write(String s) { String t = formatIfNecessary(s); writeExec(t);}
    @Override
    public void write(char[] s) { String t = formatIfNecessary(""+s); writeExec(t);}
    @Override
    public void write(int s) { String t = formatIfNecessary(""+s); writeExec(t);}


    public void writeExec(String s) { content += replaceSelfKW(s);}

    protected String formatIfNecessary(String s) {
	// if end of line with contents, comment it
	if (s.contains("\n") && lastChar() != '\n') {
	    content += comment_str;
	}
	if (s.contains("46") && Character.isLetterOrDigit(lastChar())) {
	    //	    System.out.println("46 warn : " + s);
	    s = ".";
	}

	return s;
    }

    protected String replaceSelfKW(String s) {
	if (s.contains("self")) {
	    return self_str;
	}
	return s;
    }
}
