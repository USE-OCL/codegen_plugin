/*
**
*/

package org.tzi.use.codegen.Cpp;

import org.tzi.use.codegen.*;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.mm.MNavigableElement;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MPrePostCondition;
import org.tzi.use.uml.ocl.type.Type;
import org.tzi.use.uml.ocl.expr.VarDecl;
import org.tzi.use.uml.ocl.expr.VarDeclList;
import org.tzi.use.uml.ocl.expr.Expression;

import java.io.*;
import java.util.List;


public class CppWriter extends CodeWriter {

    protected String namespace = "";
    protected String makefile = "";

    public CppWriter() {
	super();
	language = new CppLang();
    }

    public void createProject(MSystem fSystem) {
	MModel fModel = fSystem.model();
	File f = new File(fModel.name() + "/src/includes/");
	f.mkdirs();
	writeMakefile(fModel);
	writeMain(fSystem);
    }
    protected void writeMain(MSystem fSystem) {
    }

    protected void writeMakefile(MModel fModel) {
	// Create Makefile here
	makefile = "### Makefile for " +fModel.name()+" ###\n\n";
	makefile += "CC=g++\n";
	makefile += "Rm=rm -rf\n";
	makefile += "CXXFLAGS=-Wall -g -O1 -I./src/includes/\n";
	makefile += "LDFLAGS=\n";
	makefile += "\nSRC=./src/main.cpp\\\n\t\t./src/libOCl.cpp\\\n";
	for (MClass c : fModel.classes()) {
	    makefile += "\t\t./src/" +c.name()+ ".cpp\\\n";
	}
	makefile += "\n\nOBJ=$(subst .cpp,.o,$(SRC))\n";
	makefile += "DEST=key_mgr.run\n\n";

	makefile += "all: build\n\n";
	makefile += "clean: rm build\n\n";
	makefile += "build: $(OBJ)\n\t$(CXX) $(LDFLAGS) $(OBJ) -o $(DEST)\n\n";
	makefile += "rm:\n\t$(RM) $(DEST) $(OBJ)\n";

	try {
	    FileOutputStream out = new FileOutputStream(fModel.name()+ "/Makefile");
	    out.write(makefile.getBytes());
	    out.close();

// COPY libOCL TO DESTINATION FOLDER

	    InputStream is	= CppWriter.class.getResourceAsStream("/resources/libs/Cpp/libOCL.hpp");
	    FileOutputStream os	= new FileOutputStream(fModel.name()+"/src/includes/libOCL.hpp");

	    byte[] buffer = new byte[1024];
	    int length;
	    while ((length = is.read(buffer)) > 0) {
		os.write(buffer, 0, length);
	    }
	    is	= CppWriter.class.getResourceAsStream("/resources/libs/Cpp/libOCL.cpp");
	    os	= new FileOutputStream(fModel.name()+"/src/libOCL.cpp");

	    while ((length = is.read(buffer)) > 0) {
		os.write(buffer, 0, length);
	    }


	} catch (IOException e) {
	    System.out.println("File write failed - Makefile" + e);
	}

    }

    @Override
    public void beginFile(MModel fModel, MClass fClass) {
	super.beginFile(fModel, fClass);
	//	System.out.println("Beginning CPP file " + fClass.name());
	this.namespace = fModel.name();
	this.filename = this.namespace + "/src/includes/" + fClass.name() + ".hpp";

	File f = new File(this.namespace + "/src/includes/");
	f.mkdirs();

	this.fileTop += "\n#ifndef " + fClass.name().toUpperCase() + "_HPP_";
	this.fileTop += "\n#define " + fClass.name().toUpperCase() + "_HPP_\n\n";
	this.fileTop += "\n#include \"libOCL.hpp\"\n\n";

	this.content = "\nnamespace " + fModel.name() + " {\n\n";
	addTab();

    }
    public void endFile(MClass fClass) {
	rmTab();
	if (this.content.contains("std::string"))
	    this.includes = "\n#include <string>\n" + this.includes;
	this.content = this.fileTop + this.includes + this.content;
	this.content += "\n};\n#endif /* !" + fClass.name().toUpperCase() + "_HPP_ */\n";

	writeImplFile(fClass);
    }

    public void beginClass(MClass fClass) {
	String inherit = "";
	if (!fClass.parents().isEmpty()) {
	    for (MClass parent : fClass.parents()) {
		inherit += ", public " + parent.name();
	    }
	    inherit = ':' + inherit.substring(1);
	}

	this.content += tabulate() + "class " + fClass.name() + inherit + " {\n";
	addTab();
	    this.publicStatements += tabulate() +"// Constructors\n";
	this.publicStatements += tabulate() +fClass.name()+"();\n";
	this.publicStatements += tabulate() +"~"+fClass.name()+"();\n\n\n";
	rmTab();
    }
    public void endClass(MClass fClass) {
	setScope(Scope.PRIVATE);
	this.content += this.privateStatements;
	setScope(Scope.PUBLIC);
	this.content += this.publicStatements + "\n\n"+tabulate()+"// Getters and Setters\n" + this.getSet;
	this.content += "\n\n"+tabulate()+"};\n";
	rmTab();
	//	writeImplFile(fClass);
    }


    public void addOperation(MOperation fOperation) {
	// add (public) entry like [type] [operation_name] ( [args_list] ) ; \n

	addTab();
	String params = "";
	String untyped_params = "";
	for (int i=0; i<fOperation.paramList().size(); i++) {
	    if (i > 0) {
		params += ", ";
		untyped_params += ", ";
	    }
	    params += getType(fOperation.paramList().varDecl(i).type());
	    params += " ";
	    params += fOperation.paramList().varDecl(i).name();
	    untyped_params += fOperation.paramList().varDecl(i).name();

	}

	/* METHOD PROTOTYPE */
	publicStatements += tabulate()+ getType(fOperation.resultType()) + "\t" + fOperation.name() + "(";
	publicStatements += params + ");\n";

	privateStatements += "\n"+tabulate()+"void\tpre_" + fOperation.name() + "(";
	privateStatements += params + ");\n";
	privateStatements += tabulate()+"void\tpost_" + fOperation.name() + "(";
	privateStatements += getType(fOperation.resultType()) + ");\n";

	rmTab();
    }
    public void addAssociation(MAssociation fAssociation) {
	addTab();
	// find associated end
	List<MNavigableElement> ends = fAssociation.navigableEndsFrom(thisClass);
	if (ends.size() != 1) {
	    System.err.println("Associations involving "+(ends.size()+1)+" not supported yet");
	}


	MAssociationEnd associationEnd = (MAssociationEnd)ends.get(0);
	String	type = associationEnd.cls().name();		// Get acual type of associated object
	String	name = associationEnd.name();			// Get actual name of associated object

	addInclude(type);

	String	multiplicity = "" + associationEnd.multiplicity().getRanges().get(0).getUpper();	// Get actual multiplicity
	if (multiplicity.contains("-1")) {
	    multiplicity = "";
	}

	privateStatements += tabulate()+"ocl::Collection<" + type + ">\t*"+name+";\n";
	//	constructor += "\t"+name+" = new "+type+"["+multiplicity+"];\n";
	//	destructor += "\tdelete "+name+";\n";

	rmTab();
    }

    private void setScope(Scope scope) {
	if (currentScope != scope) {
	    switch (scope) {
	    case PUBLIC:
		content += "\n"+tabulate()+"public:\n";
		break;
	    case PRIVATE:
		content += "\n"+tabulate()+"private:\n";
		break;
	    case PROTECTED:
		content += "\n"+tabulate()+"protected:\n";
		break;


	    }
	    currentScope = scope;
	}

    }

    @Override
    public void writeImplFile(MClass fClass) {
    }

    @Override
    protected void addInclude(String type) {
	if (!includes.contains(type) &&
	    !this.language.values().contains(type) &&
	    !type.contains("(") &&
	    !type.contains(")") ) {
	    includes += "#include \""+type+".hpp\"\n";
	}
    }












}
