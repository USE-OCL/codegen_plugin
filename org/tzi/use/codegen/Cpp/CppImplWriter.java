/*
**
*/

package org.tzi.use.codegen.Cpp;

import org.tzi.use.codegen.*;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.mm.MNavigableElement;
import org.tzi.use.uml.mm.MAssociationEnd;
import org.tzi.use.uml.mm.MPrePostCondition;
import org.tzi.use.uml.ocl.type.Type;
import org.tzi.use.uml.ocl.expr.VarDecl;
import org.tzi.use.uml.ocl.expr.VarDeclList;
import org.tzi.use.uml.ocl.expr.Expression;

import java.io.*;

public class CppImplWriter extends CppWriter {


    protected void writeMain(MSystem fSystem) {
	String main = "/*\n * Main for " + fSystem.model().name() + "\n*/\n\n";

	for (MClass c : fSystem.model().classes()) {
	    main += "#include \"" +c.name()+ ".hpp\"\n";
	}
	main += "\n\nint\tmain(int argc, char** argv) {\n";

	// Write main content here...

	main += "\n\n\treturn 0;\n}\n";
	try {
	    FileOutputStream out = new FileOutputStream(fSystem.model().name()+ "/src/main.cpp");
	    out.write(main.getBytes());
	    out.close();
	} catch (IOException e) {
	    System.out.println("File write failed - main.cpp" + e);
	}


    }


    public void addOperation(MOperation fOperation) {
	// add (public) entry like [type] [operation_name] ( [args_list] ) ; \n
	super.addOperation(fOperation);

	String params = "";
	String untyped_params = "";
	for (int i=0; i<fOperation.paramList().size(); i++) {
	    if (i > 0) {
		params += ", ";
		untyped_params += ", ";
	    }
	    params += getType(fOperation.paramList().varDecl(i).type());
	    params += " ";
	    params += fOperation.paramList().varDecl(i).name();
	    untyped_params += fOperation.paramList().varDecl(i).name();

	}

	/* METHOD BODY */
	methodDefs += "\n\n" + getType(fOperation.resultType()) + "\t" + thisClass.name() + "::" + fOperation.name();
	methodDefs += "(" + params + ") {\n\tpre_"+fOperation.name()+"("+untyped_params+")";
	methodDefs += "\n\t"+getType(fOperation.resultType())+" result;\n\n\t// TODO : Implement method\n";

	writeOperationBody(fOperation);

	methodDefs += "\n\tpost_"+fOperation.name()+"(result);\n\treturn (result);\n}\n";

	methodDefs += "\n\nvoid\t" + thisClass.name() + "::pre_" + fOperation.name();
	methodDefs += "(" + params + ") {\n\t// TODO : Implement pre condition checks\n";
		writePreConditions(fOperation);
	methodDefs += "\n}\n";
	methodDefs += "\n\nvoid\t" + thisClass.name() + "::post_" + fOperation.name();
	methodDefs += "(" + getType(fOperation.resultType()) + " result) {\n\t// TODO : Implement post condition checks\n";
		writePostConditions(fOperation);
	methodDefs += "\n}\n";

    }

        @Override
	public void writeImplFile(MClass fClass) {
	String cppContent = "";

	cppContent = "/*\n** Source file generated with USE codegen plugin\n*/\n\n";
	cppContent += "\n#include \"" + fClass.name() + ".hpp\"\n\n";

	cppContent += "using namespace " + namespace + ";\n\n";

	cppContent += fClass.name() + "::" + fClass.name() + " () {\n";
	cppContent += constructor;
	cppContent += "}\n\n";
	cppContent += fClass.name() + "::~" + fClass.name() + " () {\n";
	cppContent += destructor;
	cppContent += "}\n\n";

	cppContent += methodDefs;

	try {
	    FileOutputStream out = new FileOutputStream(namespace + "/src/" + fClass.name() + ".cpp");
	    out.write(cppContent.getBytes());
	    out.close();
	} catch (IOException e) {
	    System.out.println("File write failed - " + fClass.name() + ".cpp");
	}
    }


        /******** CALLS TO WRITE ACTUAL CODE *********/
    private void writePreConditions(MOperation fOperation) {
	//	System.out.println(fOperation.preConditions().get(0).name());
	if (fOperation == null)
	    return ;

	methodDefs += "\ttry {\n";

	// for each pre_condition...
	for (MPrePostCondition fPreCondition : fOperation.preConditions()) {
	    methodDefs += "\n\t\tif ( ! (\t" + writeExpression(fPreCondition.expression()) + "\t) ) {\n";
	    methodDefs += "\t\t\t throw ConstraintViolated(\""+fOperation.name()+"("+fOperation.paramNames()+")@pre\", ";
	    methodDefs += "\""+writeExpression(fPreCondition.expression())+"\");\n\t\t}\n";
	}

	methodDefs += "\n\t} catch (ocl::ConstraintViolated const& e) {\n";
	methodDefs += "\t\tstd::cerr << \"ERROR : Constraint violated !\\n\" << e.what() << std::endl;\n";
	methodDefs += "\t\tif (e.fatal())\n\t\t\texit(e.code());\n";
	methodDefs += "\t}\n";
	// write somethoing like : if (precondition unsatisfied) {throw exception}
    }

    private void writeOperationBody(MOperation fOperation) {
	//	System.out.println(fOperation.expression().name());
	//	System.out.println(fOperation.getStatement());
	if (fOperation == null)
	    return ;
	methodDefs += writeExpression(fOperation.expression());
    }

    private void writePostConditions(MOperation fOperation) {
	//	System.out.println(fOperation.postConditions().get(0).name());
	if (fOperation == null)
	    return ;
	methodDefs += "\ttry {\n";
	// for each pre_condition...
	for (MPrePostCondition fPostCondition : fOperation.postConditions()) {
	    methodDefs += "\n\t\tif ( ! (\t" + writeExpression(fPostCondition.expression(), true) + "\t) ) {\n";
	    methodDefs += "\t\t\t throw ConstraintViolated(\""+fOperation.name()+"("+fOperation.paramNames()+")@post\", ";
	    methodDefs += "\""+writeExpression(fPostCondition.expression())+"\");\n\t\t}\n";
	}

	methodDefs += "\n\t} catch (ocl::ConstraintViolated const& e) {\n";
	methodDefs += "\t\tstd::cerr << \"ERROR : Constraint violated !\\n\" << e.what() << std::endl;\n";
	methodDefs += "\t\tif (e.fatal())\n\t\t\texit(e.code());\n";
	methodDefs += "\t}\n";
	// write somethoing like : if (postcondition unsatisfied) {throw exception}exception}}
    }

    private String writeExpression(Expression expr) {
	return writeExpression(expr, false);
    }

    private String writeExpression(Expression expr, boolean isPostCondition) {

	if (expr == null)
	    return "//error\n";
	try {
	    CppExpressionWriter exprVisitor = new CppExpressionWriter();
	    exprVisitor.setIsPostCondition(isPostCondition);
	    expr.processWithVisitor(exprVisitor);
	    return exprVisitor.toString();
	} catch (IOException e) {
	    System.err.println("Error opening file for CppExpressionWriter");
	}

	return "//error\n";
    }


}
