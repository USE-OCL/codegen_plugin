/*
**
*/

package org.tzi.use.codegen.Cpp;

import org.tzi.use.codegen.*;


import java.util.HashMap;


public class CppLang extends HashMap<String, String>{

    public CppLang() {
	super();
	this.put("NAME",	"Cpp");
	this.put("COMMENT",	"//");
	this.put("THIS",	"this");

	this.put("LINK_ATTR",	".");
	this.put("LINK_OP",	".");

	this.put("IF",		"\nif");
	this.put("ELSE",	"else");
	this.put("WHILE",	"\nwhile");

	this.put("COND_OPEN",	"(");
	this.put("COND_CLOSE",	")");
	this.put("ARG_OPEN",	"(");
	this.put("ARG_CLOSE",	")");
	this.put("BLOCK_OPEN",	"{\n");
	this.put("BLOCK_CLOSE", "\n}\n");

	this.put("END_LINE",	";\n");
	this.put("VAR_SEPR",	",");

	this.put("INTEGER",	"int");
	this.put("BOOLEAN",	"bool");
	this.put("CHAR",	"char");
	this.put("STRING",	"std::string");

	this.put("RETURN",	"return ");
	this.put("RES_EQ",	"result = ");
	this.put("DECL_FUNC",	"");
	this.put("DECL_VAR",	"");


	this.put("INT",		"int");
	this.put("STRING",	"std::string");
	this.put("BOOL",	"bool");
	this.put("CHAR",	"char");
	this.put("REAL",	"double");
	this.put("VOID",	"void");

	this.put("COLLECTION",	"ocl::Collection");
	this.put("SEQUENCE",	"ocl::Sequence");
	this.put("SET",		"ocl::Set");
	this.put("BAG",		"ocl::Bag");



    }

}
