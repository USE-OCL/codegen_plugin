/*
**
*/

package org.tzi.use.codegen.Cpp;


import org.tzi.use.uml.ocl.expr.Expression;
import org.tzi.use.uml.ocl.expr.ExpressionPrintVisitor;
import org.tzi.use.uml.ocl.expr.*;

import org.tzi.use.codegen.*;

import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;


public class CppExpressionWriter extends CodeExpressionWriter {

    public CppExpressionWriter() throws IOException{
	super(new CppLang());
    }

    /*
    ** Check for returns : in OCL, like ruby, just expression is writen, without 'return'
    ** if valeu_epression & parent != affectation/calculation/condition/comparison
    ** then prepend with 'return'
    */



    @Override
    public void visitEmptyCollection(ExpEmptyCollection exp) {
	((VirtualPrintWriter)writer).write("ocl::Collection<"+type(exp.type().toString(), exp)+")");
	System.out.println("empty collect : " + exp);
	//	((VirtualPrintWriter)writer).writeExec("*");
    }

    @Override
    public void visitCollectionLiteral(ExpCollectionLiteral exp) {
	boolean isOwnLine = false;
	if (((VirtualPrintWriter)writer).lastChar() == '\n') {
	    isOwnLine = true;
	    ((VirtualPrintWriter)writer).writeExec("\tresult = ");
	}
	//	((VirtualPrintWriter)writer).writeExec(keyword(exp.getKind(), exp));
	((VirtualPrintWriter)writer).writeExec(operator("{", exp));

	boolean first = true;
	for (Expression elemExp : exp.getElemExpr()) {
	    if (!first) {
		((VirtualPrintWriter)writer).writeExec(",");
		((VirtualPrintWriter)writer).writeExec(ws());
	    }
	    elemExp.processWithVisitor(this);
	    first = false;
	}
	((VirtualPrintWriter)writer).writeExec(operator("}", exp));

	if (isOwnLine) {
	    ((VirtualPrintWriter)writer).writeExec(";\n");
	}
    }


    	@Override
	public void visitAsType(ExpAsType exp) {
	    cast(exp);
	}


    @Override
    public void visitConstString(ExpConstString exp) {
	writer.write("\"");
	writer.write(literal(exp.value(), exp));
	writer.write("\"");
    }



    @Override
    public void visitQuery(ExpQuery exp, VarInitializer accuInitializer) {
	/*
	 * Build expression like "OCL::QUERY_NAME(MATCH_BLOCK, POINTER_TO_FN)"
	 */

	// shunt
	super.visitQuery(exp, accuInitializer);
	return;
	/*
	((VirtualPrintWriter)writer).writeExec("\n");
	exp.getVariableDeclarations().processWithVisitor(this); // works if there is only one...
	writer.write(operator(".", exp));
	writer.write(operation(exp.name(), exp));
	writer.write(operator("(", exp));
	writer.write(ws());

	writer.write(ws());
	/* this is the actuall operation : write lambda function *
	((VirtualPrintWriter)writer).writeExec("[]( /* Arguments * ) {\n");
	exp.getQueryExpression().processWithVisitor(this);
	((VirtualPrintWriter)writer).writeExec("\n}");

	writer.write(ws());
	writer.write(operator(")", exp));
*/
    }

}
