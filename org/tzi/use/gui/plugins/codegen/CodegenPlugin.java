package org.tzi.use.gui.plugins.codegen;

import org.tzi.use.runtime.IPlugin;
import org.tzi.use.runtime.IPluginRuntime;
import org.tzi.use.runtime.impl.Plugin;

public class CodegenPlugin extends Plugin implements IPlugin {

    final protected String PLUGIN_ID = "CodegenPlugin";

    public String getName() {
	return this.PLUGIN_ID;
    }

    public void run(IPluginRuntime pluginRuntime) throws Exception {

    }

}
