/*
**
 */

package org.tzi.use.gui.plugins.codegen;

import java.util.Collection;

import org.tzi.use.config.Options;
import org.tzi.use.gui.main.MainWindow;

import org.tzi.use.main.Session;
import org.tzi.use.runtime.gui.IPluginAction;
import org.tzi.use.runtime.gui.IPluginActionDelegate;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.sys.MSystemException;
import org.tzi.use.util.Log;

import org.tzi.use.codegen.CodeWriter;



public abstract class GenHeaders implements IPluginActionDelegate {


    protected CodeWriter writer;

    public GenHeaders() {

    }



    public void performAction(IPluginAction pluginAction) {
	final Session fSession = pluginAction.getSession();
	final MainWindow fMainWindow = pluginAction.getParent();
	MSystem fSystem = fSession.system();

	/*
	** Do stuff here
	*/

	writer.createProject(fSystem);

	//		MModel fModel = fSystem.model();
	Collection<MObject> fObjects = fSystem.state().allObjects();
	Collection<MClass> fClasses = fSystem.model().classes();

	//	System.out.println("Plugin is called...");
	//	System.out.println(fSystem);

	for (MClass fClass : fClasses) {
	    // if visible, skip....
	    //	    if (!fClass.isVisible())
	    //		continue;

	    System.out.println("Generating code for class " + fClass.name());

	    // Create file for this class, create object, inheriting stuff...
	    writer.beginFile(fSystem.model(), fClass);
	    writer.beginClass(fClass);

	    // handle elements of class
	    //System.out.println("\nAttributes");
	    for (MAttribute fAttribute : fClass.attributes()) {
		// do stuff with attributes
		//	System.out.println("\t" + fAttribute);
		writer.addAttribute(fAttribute);

	    }

	    //	    System.out.println("\nOperations");
	    for (MOperation fOperation : fClass.operations()) {
		// do stuff with attributes
		//	System.out.println("\t" + fOperation);
		writer.addOperation(fOperation);
	    }

	    //	    System.out.println("\nAssociations");
	    for (MAssociation fAssociation : fClass.associations()) {
		// do stuff with assocaitions
		//	System.out.println("\t" + fAssociation);
		writer.addAssociation(fAssociation);
	    }

	    writer.endClass(fClass);
	    writer.endFile(fClass);

	    writer.write();
	}

	System.out.println("Headers generation finished");

    }

}
