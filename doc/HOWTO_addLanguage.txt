Adding a new language to GenCode



There are several steps for this.

Note - the [Language] tag replaces any occurence of your languages name. For example, if you implement PHP, read Gen[Language] as GenPHP


1 - First, register the plugin
	add an entry in the useplugin.xml file - This entry defines a menu entry/name/icon, and a class to call on click (org.tzi.use.gui.plugins.codegen.Gen[Language])

2 - Create your java class org.tzi.use.gui.plugins.codegen.Gen[Language])
	This class must extend org.tzi.use.gui.plugins.codegen.GenHeaders
	The only code to insert into this class is a constructor, which should look something like this :
    public Gen[Language]() {
	       super();
	       this.writer = new [Language]Writer();
	}

3 - Implement the writer
	Writers go in org.tzi.use.codegen. They must extend org.tzi.use.codegen.CodeWriter, and implement it's abstract methods (see org.tzi.use.codegen.CppWriter for an example)

Then, it should work !
